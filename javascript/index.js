// index.js
// script entry point

// imports and requires
const textData = require('./textData');
const fileReader = require('../javascript/fileReader');

// first read the data from the text file
fileReader.data.readAndSaveDataFromTxtFile().then(function (result) {

    // sort the data into arrays for easier access
    let arrays = textData.data.sortDataIntoArrays(result);

    // create objects from the arrays according to worker
    let arrayOfObjects = textData.data.createObjectsFromArrays(arrays);

    // answers questions one and two  by sifting through each worker's intervals
    let collectionOfDateValues = textData.data.convertIntervalsToDateAndCompare(arrayOfObjects);

    // splits each worker's intervals into pairs for easier comparison 
    let pairingCollection = textData.data.splitIntervalsIntoPairs(arrayOfObjects);

    // answers question three by comparing each individual interval pair with each other
    let objectWithQuestionThreeAnswer = textData.data.findOverlappingIntervals(pairingCollection, collectionOfDateValues);

    // print results
    console.log("Question 1 - Earliest Date: " + collectionOfDateValues.earliestDate.toISOString() + "\n" +
        "Question 2 - Latest Date: " + collectionOfDateValues.latestDate.toISOString());
    console.log("Question 3 - Overlapping Worker Intervals: " + "\n");

    objectWithQuestionThreeAnswer.overlappingWorkerDateIntervals.forEach(elem => {
        console.log(elem + "\n");
    });
});