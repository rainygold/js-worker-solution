// fileReader.js
// handles all file operations

// imports and requires
const fs = require('fs');
const file = ('./resources/input.txt');

// for exporting functions
var methods = {};

// saves data from input.txt to a variable
methods.readAndSaveDataFromTxtFile = async function readAndSaveDataFromTxtFile() {

    // promise for asynchronous file reading
    return new Promise(function (resolve, reject) {
        fs.readFile(file, 'utf8', function (err, data) {
            if (err)
                reject(err);
            else
                resolve(data);

        });
    });
};

exports.data = methods;
