// data.js
// handles all data manipulation operations

// for exporting functions
var methods = {};

// sorts input data into objects for easier access
methods.sortDataIntoArrays = function sortDataIntoArrays(data) {

    // split the data by worker
    return data.toString().split('\n');
};

// creates an array of objects from sorted arrays
methods.createObjectsFromArrays = function createObjectsFromArrays(data) {

    // mother array (to hold the other objects)
    let motherArray = [];

    // create an object for each index (worker) of the input data
    data.forEach(element => {

        let childObject = {
            id: findId(element),
            intervals: findTimeDateIntervals(element),
        };

        // add to the array
        motherArray.push(childObject);
    });

    return motherArray;
};

// finds the worker id in a given string
function findId(inputString) {

    return inputString.split('@', 1).toString();
};

// finds the time/date intervals in a given string
function findTimeDateIntervals(inputString) {

    // remove the id and return the date/times as an array for easier iteration
    return inputString.substring(inputString.indexOf('@') + 1, inputString.length).split(',');
};

// converts date/time intervals to Dates, compares them, and saves them as a returned object
methods.convertIntervalsToDateAndCompare = function convertIntervalsToDateAndCompare(inputArray) {

    // checker variables
    let earliestDate;
    let latestDate;

    // object for collecting the final values
    let collectionOfDateValues = {};

    // iterate through each object in the input array to compare all dates
    inputArray.forEach(worker => {

        worker.intervals.forEach(interval => {

            // split the intervals
            let splitInterval = interval.split('/');

            // remove problematic characters (square brackets)
            const splitIntervalReplaced = splitInterval.map(arrayContents => arrayContents.replace(/[[\]]/g, ''));

            splitIntervalReplaced.forEach(split => {

                // convert to JS Date format for comparison
                let intervalAsDate = new Date(split);

                // find and assign the relevant values
                if (earliestDate === undefined || intervalAsDate < earliestDate)
                    earliestDate = intervalAsDate;
                else if (latestDate === undefined || intervalAsDate > latestDate)
                    latestDate = intervalAsDate;
            });
        })
    });

    // store the collected values in the object
    collectionOfDateValues.earliestDate = earliestDate;
    collectionOfDateValues.latestDate = latestDate;

    return collectionOfDateValues;
};

// finds the date/time intervals where two workers are free together
methods.findOverlappingIntervals = function findOverlappingIntervals(inputArray, inputObject) {

    // collection of overlapping intervals
    let overlappingIntervalsCollection = [];

    inputArray.forEach(worker => {

        // checker variables
        let firstDate;
        let secondDate;
        let workerId = worker.id;

        // lock in the pair of dates
        worker.intervalPairings.forEach(intervalPair => {

            // order is randomised, so find the correct order
            if (intervalPair.partOne > intervalPair.partTwo) {
                firstDate = intervalPair.partTwo;
                secondDate = intervalPair.partOne;
            } else if (intervalPair.partTwo > intervalPair.partOne) {
                firstDate = intervalPair.partOne;
                secondDate = intervalPair.partTwo;
            }

            // now check the other workers' pairs and see if there's an overlap
            let checkForOverlap = searchIntervalPairs(firstDate, secondDate, workerId, inputArray);
            if (checkForOverlap.length > 0)
                overlappingIntervalsCollection.push(checkForOverlap);
        });

    });


    // remove duplicate entries
    const flattenedCollection = [...new Set(overlappingIntervalsCollection.flat().map(date => date.toString()))];
    let collectionOfDates = [];
    flattenedCollection.map(string => {
        collectionOfDates.push(string.split(',').map(string => new Date(string).toISOString()))
    })

    // store the collected values in the object
    inputObject.overlappingWorkerDateIntervals = collectionOfDates;

    return inputObject;
};

// searches through workers' pairs of dates to find overlapping values (Avoids input's worker's pairs)
function searchIntervalPairs(firstDate, secondDate, workerId, inputArray) {
    // checker variables
    let resultCollection = [];
    inputArray.forEach(worker => {
        // proceed if the worker differs from the original
        if (worker.id != workerId) {
            worker.intervalPairings.forEach(pair => {
                let pairResult = [];
                // need to be within the same range
                if (pair.partTwo <= secondDate && pair.partTwo > firstDate) {
                    // handle the beginning of the overlap
                    if (pair.partOne < firstDate)
                        pairResult.push(firstDate);
                    else
                        pairResult.push(pair.partOne);
                    // handle the end of the overlap
                    if (pair.partTwo < secondDate)
                        pairResult.push(pair.partTwo);
                    else
                        pairResult.push(secondDate);
                }
                if (pairResult.length == 2)
                    resultCollection.push(pairResult);
            });
        }
    });
    return resultCollection;
}

// splits the input's intervals into pairs for accessibility
methods.splitIntervalsIntoPairs = function splitIntervalsIntoPairs(inputArray) {

    inputArray.forEach(worker => {

        let pairings = [];

        worker.intervals.forEach(intervals => {

            let pair = {};

            // split by each set of intervals
            let splitIntervals = intervals.split(',');

            // remove problematic characters (square brackets)
            const splitIntervalReplaced = splitIntervals.map(arrayContents => arrayContents.replace(/[[\]]/g, ''));

            splitIntervalReplaced.forEach(eachSplit => {

                // then split by the pairings
                let splitPairings = eachSplit.split('/');

                // convert to a Date
                splitPairings.forEach(pairPart => {
                    let pairPartAsDate = new Date(pairPart);

                    if (pair.partOne === undefined)
                        pair.partOne = pairPartAsDate;
                    else
                        pair.partTwo = pairPartAsDate;
                });

                // keeps them separated from the other pairs
                pairings.push(pair);

                // add new pairs to the worker
                worker.intervalPairings = pairings;
            });
        });
    });

    // return updated object
    return inputArray;
};

exports.data = methods;