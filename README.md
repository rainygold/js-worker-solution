# Build
* Terminal command: 'node javascript/index.js'
* Testing: 'npm test'

# Tools
* OS: Linux (with kernel 5.0.16)
* Editors: Visual Studio Code
* Nodejs: 12.4.0
* Testing: Mocha

# Ethos
A CLI program produced using JavaScript (Node) that answers three given questions about a supplied data file (resources/input.txt)

Questions:
* What is the starting date and time (in UTC) of the earliest interval where any of the workers are free?
* What is the ending date and time (in UTC) of the latest interval where any of the workers are free?
* What are the intervals of date and times (in UTC) where there are at least 2 workers free?

# Testing
All testing was written using the 'should' and 'mocha' dependencies.

# Improvements
* Using the test case(s), the solution returns the correct answers for questions 1 and 2, but 3 returns the correct answer with extra elements. Most likely faulty logic (check lines 147-158 in textData.js)
* More extensive testing with negative testing
* Further usage of standard built-in functional methods (e.g. map, filter, reduce) particularly with regard to the 'Interval Functions' in textData.js.