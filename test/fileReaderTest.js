// fileReaderTest.js
// contains all tests for fileReader.js

// imports and requires
const should = require('should');
const fileReader = require('../javascript/fileReader');

describe('fileReader.js Tests', function () {

    describe('readAndSaveDataFromTxtFile', function () {
        it('returns data from a local .txt file', function () {

            // retrieve the data
            fileReader.data.readAndSaveDataFromTxtFile().then(function (data) {

                // check the data is not undefined
                should(data).be.a.String();
            });

        });
    });
});