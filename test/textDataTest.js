// textDataTest.js
// contains all tests for textData.js

// imports and requires
const should = require('should');
const textData = require('../javascript/textData');
const fileReader = require('../javascript/fileReader');

describe('textData.js Tests', function () {

    // these functions are interdependent

    describe('sortDataIntoArrays', function () {

        it('sorts input data into arrays for future operations', function () {

            // these tests require data from fileReader.js
            fileReader.data.readAndSaveDataFromTxtFile().then(function (data) {

                // sort the data
                const sortedData = textData.data.sortDataIntoArrays(data);

                // check the data is made up of arrays
                should(sortedData).be.an.Array();
            });
        });
    });

    describe('createObjectsFromArrays', function () {

        it('creates an array of objects from sorted arrays', function () {

            // these tests require data from fileReader.js
            fileReader.data.readAndSaveDataFromTxtFile().then(function (data) {

                // sort the data
                const sortedData = textData.data.sortDataIntoArrays(data);

                // 'objectify' the sorted data
                const objectSortedData = textData.data.createObjectsFromArrays(sortedData);

                // check data type
                should(objectSortedData).be.an.Array();

                // check contents of the data
                objectSortedData[0].id.should.be.a.String();
            });
        });
    });

    describe('convertIntervalsToDateAndCompare', function () {
        it('converts date/time intervals to Dates, compares them, and saves them as a returned object', function () {

            // these tests require data from fileReader.js
            fileReader.data.readAndSaveDataFromTxtFile().then(function (data) {

                // sort the data
                const sortedData = textData.data.sortDataIntoArrays(data);

                // 'objectify' the sorted data
                const objectSortedData = textData.data.createObjectsFromArrays(sortedData);

                // returns an object with questions one and two answered
                const convertedData = textData.data.convertIntervalsToDateAndCompare(objectSortedData);

                // check the type of the result
                should(convertedData).be.a.Object();

                // check the content of the result
                convertedData.earliestDate.should.be.a.Date();
                convertedData.latestDate.should.be.a.Date();
            });
        });
    });

    describe('splitIntervalsIntoPairs', function () {

        it('splits the input\'s intervlas into pairs for accessibility', function () {

            // these tests require data from fileReader.js
            fileReader.data.readAndSaveDataFromTxtFile().then(function (data) {

                // sort the data
                const sortedData = textData.data.sortDataIntoArrays(data);

                // 'objectify' the sorted data
                const objectSortedData = textData.data.createObjectsFromArrays(sortedData);

                // returns an object with questions one and two answered
                const convertedData = textData.data.convertIntervalsToDateAndCompare(objectSortedData);

                // returns the object with split up pairs of intervals added
                const splitIntervalCollection = textData.data.splitIntervalsIntoPairs(objectSortedData);

                // check the type of the result
                should(splitIntervalCollection).be.a.Object();

                // check the content of the result
                splitIntervalCollection[0].intervalPairings.should.be.an.Array();

            });
        });
    });

    describe('findOverlappingIntervals', function () {

        it('finds the date/time intervals where two workers are free together', function () {

            // these tests require data from fileReader.js
            fileReader.data.readAndSaveDataFromTxtFile().then(function (data) {

                // sort the data
                const sortedData = textData.data.sortDataIntoArrays(data);

                // 'objectify' the sorted data
                const objectSortedData = textData.data.createObjectsFromArrays(sortedData);

                // returns an object with questions one and two answered
                const convertedData = textData.data.convertIntervalsToDateAndCompare(objectSortedData);

                // returns the object with split up pairs of intervals added
                const splitIntervalCollection = textData.data.splitIntervalsIntoPairs(objectSortedData);

                // returns an object with question three answered
                const collectionWithOverlappingIntervals = textData.data.findOverlappingIntervals(splitIntervalCollection, convertedData);

                // check the type of the result
                should(collectionWithOverlappingIntervals).be.an.Object();

                // check the content of the result
                collectionWithOverlappingIntervals.overlappingWorkerDateIntervals.should.be.an.Array();
            });
        });
    });
});